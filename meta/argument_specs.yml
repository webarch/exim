# Copyright 2018-2025 Chris Croome
#
# This file is part of the Webarchitects Exim Ansible role.
#
# The Webarchitects Exim Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Exim Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Exim Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
argument_specs:
  main:
    author: Chris Croome
    description: Ansible role for installing Exim for outgoing email on Debian and Ubuntu.
    short_description: The main entry point for the exim role.
    options:
      exim:
        type: bool
        required: true
        description: Run the tasks in this role
      exim_cert_path:
        type: str
        required: false
        description: Path to a x509 certificate for the exim_mailname.
      exim_key_path:
        type: str
        required: false
        description: Path to a x509 key for the exim_mailname.
      exim_dkim_domains:
        type: dict
        required: false
        description: Dictionay of domains and settings for DKIM.
      exim_dc_eximconfig_configtype:
        type: str
        required: true
        description: The main configuration type.
        choices:
          - internet
          - smarthost
          - satellite
          - local
          - none
      exim_dc_localdelivery:
        type: str
        required: true
        description: Name of the default transport for local mail delivery.
      exim_dc_local_interfaces:
        type: str
        required: true
        description: Semicolon-seperated list of IP addresses the Exim daemon should listen on.
      exim_dc_minimaldns:
        type: bool
        required: true
        description: Boolean option to activate some option to minimize DNS lookups.
      exim_dc_other_hostnames:
        type: str
        required: true
        description: This is the list of domains, semicolon-separated, for which this machine should consider itself the final destination.
      exim_dc_readhost:
        type: str
        required: true
        description: For "smarthost" and "satellite" it is possible to hide the local mailname in the headers of outgoing mail and replace it with this value instead, using rewriting.
      exim_dc_relay_domains:
        type: str
        required: true
        description: A semicolon-separated list of domains for which we accept mail from anywhere on the Internet but which are not delivered locally, e.g. because this machine serves as secondary MX for these domains.
      exim_dc_relay_nets:
        type: str
        required: true
        description: A semicolon-separated list of machines for which we serve as smarthost. Please note that 127.0.0.1 and ::1 are always permitted to relay since /usr/lib/sendmail is available anyway and relay control doesn't make sense here.
      exim_dc_smarthost:
        type: str
        required: true
        description: A semicolon-separated list of hosts to which all outgoing mail is passed to and that takes care of delivering it.
      exim_hide_mailname:
        type: bool
        required: true
        description: Boolean option that controls whether the local mailname in the headers of outgoing mail should be hidden, only effective for “smarthost” and “satellite”
      exim_mailname:
        type: str
        required: true
        description: The primary domain name of the server, defaults to inventory_hostname.
      exim_use_split_config:
        type: bool
        required: true
        description: Boolean option that controls whether update-exim4.conf uses /etc/exim4/exim4.conf.template (false) or the multiple files below /etc/exim4/conf.d (true) as input.
      exim_packages:
        type: list
        required: true
        description: A list of .deb packages to be installed.
      exim_cert_backup:
        type: bool
        required: true
        description: Copy, rather than delete /etc/exim4/exim.crt and /etc/exim4/exim.key when they are files and have expired.
...
