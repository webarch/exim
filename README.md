# Webarchitects Exim Ansible Role

This repository contains an Ansible role for installing [Exim](https://exim.org/) on Debian servers and configuring it to allow applications to send DKIM signed outgoing email, but not accept incoming email.

See the [defaults/main.yml](defaults/main.yml) file for the default variables and the [meta/argument_specs.yml](meta/argument_specs.yml) file for descriptions of these variables, see also the [Debian update-exim4.conf manpage](https://manpages.debian.org/bullseye/exim4-config/update-exim4.conf.8.en.html) for more detailed descriptions for the `exim_dc_` prefixed variables.

## Outgoing email

This role enables two outgoing email methods by default:

* `/usr/sbin/sendmail`
* Unauthenticated, plain text SMTP to `localhost` on port `25`

In addition if a certificate matching the `exim_mailname` is found at `exim_cert_path` then an additional method is enabled:

* Unauthenticated TLS SMTP to `localhost` on port `25`

You can test outgoing SMTP email using `mutt` are this `~/.muttrc` settings:

```muttrc
set smtp_url=smtp://localhost:25/
set ssl_verify_host=no
set ssl_starttls=yes
set ssl_force_tls=no
```

For `/usr/sbin/sendmail` comment the `smtp_url` and use:

```muttrc
set sendmail=/usr/sbin/sendmail
```

## Certificates

The [Apache role](https://git.coop/webarch/apache) defaults to use `mod_md` to provision a cer for the `inventory_hostname` and it copies this key and cert for Exim.

## DKIM

This role generates a default signing DKIM key and also generates the DNS records needed and displays them and when they have been set DKIM is enabled.

Additional domains can be added via the `exim_dkim_domains` dictionary, if the private kay has been created already best use Ansible vault to encrypt this dictionary, for example:

```yml
exim_dkim_domains:
  "{{ exim_mailname }}":
    default: true
  example.org:
    selector: 20210319
    privatekey: |
      -----BEGIN RSA PRIVATE KEY-----
      MIIEjQIBAAKB/QNnBfU6m2OXb4l364bBhs3XMJBWi4QqeKQHcgfBqWDGh/MjMqGM
      U3GIeul5PJAHwetQFgS442rPDvTt7JgfMysnns3DOBsYG4+BpGJe76qanBG2erYf
      vjWQtqmUx/NF4SLIS20jwb8tQazWp5I6sUGyNpGz0qHBzkzKWHvEJIszpJxyvad9
      i6M9aVELqGMF/XePbsA2Ar18VbVQX2LN9bwv6SXQWXSdBPbLwHLVxxg1LmpVpPyo
      S6qVfIcDCbPCL75RV4gutq7M/dad/IwCUL1dGdyhOXgSKwKGB0OcExb72sG9ozZ1
      DSv5R+qcplQeNiNQhqFD+i6EuxDcnx8CAwEAAQKB/QLr3BO4ZF7YUW4IxvMHCwWF
      tC7aACRUiwzbT68VOqbbDzNVj01hvOuIMXBxJezMnFW8h4s6bBjy+3aVKkVa1IZD
      GHi3G184DcHjoA7OUJ187m/O7AGfs6MuQ4fLNPrNjwZcT1bAEsRgyTjrfSlyt2se
      5venHDzCuKz6N4rc2Dh6DNFsUhTbqFNijYBJd+M7gF3p0IafKElQWexdKa/OpoK8
      KEdxk037AoGzOiMY6PCQIumrEu1pljAv89xdiWvggYVnT5OqLYd3H2vizj9xzMR6
      Nehf2UishbV8ZkFnWyRQyBm5rzC4YU2W4a4iYIX5HKCJgmbcs/MlAi+kNVECfwHc
      cuCjtCok888eiaa8ceAAuqhMMJmAVi4j2nQsAFX1xicSX26E6lpCd2sxHKlo81L1
      JgJGtBqwiVrPH1hZx8cP3CXzL7GLo2ZztsAcwOB/KsV4bs79zIx/zhMm+IpMKRSN
      3XV8IyrU53I2MTFLZLV3oszeNVKV40B8JUTlSKsCfwHUAh77hjaTSQbQ79SR3w7p
      QZDTGIQMa2+0nwvvc4u4Hp2cXTb0QNGD0Im2cnEIZHcKGK9585COjJxE95KLIbnX
      ueLrzd9POQSKRY4AMyJMoqQyQOSKE8Zpj7m9zB7Eg9/2iTqYgCJyolYeQ5F0WR7u
      C5wUmlWJIDPPLsQVq10CfwHT+rJTmX9b1G64XBR+PIv9q1nrnEDqIbwoU0aQgbPt
      OLjf/wSbYWVOq+WJ09FC2N2/FZpewEEG7aNYlj090lDgbyiHeW0270SOnA6PL5RS
      1DwqUwzVlbjbQBrnyHBu5lreDYx8Zj6H6N3yBNeaak0kObBpAo8f0ptgVWsamwkC
      fwCfBp16gC95Op47TtMfQfkyH1pTsIfmJHePm1+V3OBEDvslT7NFqeTHcovo19g6
      rtCYOK0ftjiPh43uDWXZKvuYJwzJbWoM5GX7igjnXniwK6+5d71wgamw4uqNaJen
      1Nu8TQMBIMETzL+Qhwscys+suXsusQaRksSoCM8OQe0CfwHbbtCH1GcYf3swbkmq
      3jdubNfcpWeWJAqkFTpdNKoNf3Er8n1p3N8JMJlfqWtgpDwv3JY0zN4S8yVHwTEc
      5iR50el4rauDkxxijUAZ3jNoOBx8+P2BjJ1Od2Kky8zVhKVr0UC8gq/g877B87RR
      e5NSt3oShRmVJS5sHJZDniY=
      -----END RSA PRIVATE KEY-----
```

If the `selector` is not defined then one based on the date will be generated, if the `privatekey` is not defined then one will be generated.

## Copyright

Copyright 2018-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
